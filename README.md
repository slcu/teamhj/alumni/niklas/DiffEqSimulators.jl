# DiffEqSimulators.jl

This is a small (unregistered) library of functions which supply a common API for doing a number of different simulations using [DifferentialEquations.jl](https://github.com/JuliaDiffEq/DifferentialEquations.jl).

This is (somewhat) useful for software carpentry as it allows you to toggle between different kinds of simulations (ODE, SDE, MC, etc) in your code by just changing a pointer to a function. This allows for an easy way of switching simulation mode using a kwarg or using something like [Interact.jl](https://github.com/JuliaGizmos/Interact.jl).

Some of this has just emerged due to an immediate need and has not been very well thought out. If I still find this to be useful in the future I'm likely to do some cleaning which breaks backwards compatibility.


Common API:

```julia
using DiffEqSimulators

sol = simulator(ode, noise_function, u0, tspan, params; callback=nothing)
```
here, `sol` is the solution object given by DiffEq's `solve` function. 
